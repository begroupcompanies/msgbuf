package msgbuf

import (
	"encoding/json"
	"errors"
	"strconv"
	"sync"
	"time"
)

var mBuf MessageBuffer
var once sync.Once
var msgChan chan []string

type MessageBuffer struct {
	m     [][]string
	total int
	size  int
	limit time.Duration
	sync.RWMutex
}

// Put помещает строку или слайс строк в канал сообщений
// для дальнейшего размещения в буфере
func (buf *MessageBuffer) Put(val ...string) {
	msgChan <- val
}

// При первом вызове:
// - Создается новый MessageBuffer размера totalDuration / duration,
// 	 totalDuration - максимальное время хранения каждого сообщения в буфере
// 	 duration - время
// - Запускается timeTicker по которому будет обновлятся содержимое буфера
// При последующих вызовах:
// - Возвращается уже существующий MessageBuffer
// При каждом срабатывании timeTicker'а содержимое буфера сдвигается вправо,
// т.е. в начале буфера появляется пустая ячейка, а конечная ячейка выталкивается
// Сообщения из канала поступают в начальную ячейку буфера
func Run(totalDuration time.Duration, duration time.Duration) (*MessageBuffer, error) {
	if duration == 0 || totalDuration == 0 {
		return nil, errors.New("Invalid durations")
	}
	if duration > totalDuration {
		return nil, errors.New("Total duration < duration")
	}
	once.Do(func() {
		flushTicker := time.NewTicker(duration)
		msgChan = make(chan []string)
		size := int(totalDuration / duration)
		mBuf.size = size
		mBuf.m = make([][]string, size)
		mBuf.limit = duration
		var temp [][]string
		go func() {
			for {
				select {
				case <-flushTicker.C:
					temp = make([][]string, 1)
					mBuf.Lock()
					mBuf.m = append(temp, mBuf.m[0:len(mBuf.m)-1]...)
					mBuf.Unlock()
				case msg := <-msgChan:
					mBuf.Lock()
					mBuf.m[0] = append(mBuf.m[0], msg...)
					mBuf.Unlock()
				}
			}

		}()
	})
	return &mBuf, nil
}

// Json преобразовывает текущее состояние буфера в формат json с полями:
// - `messages` (сообщения разбитые по временным интервалам)
// - `total` (общее количество сообщений в буфере)
func (buf *MessageBuffer) Json() ([]byte, error) {
	buf.RLock()
	m := make(map[string][]string)
	for k, v := range buf.m {
		key := time.Now().Add(-(time.Duration(k) * mBuf.limit)).Unix()
		m[strconv.FormatInt(key, 10)] = v
	}
	buf.RUnlock()

	jsn, err := json.Marshal(struct {
		Messages map[string][]string `json:"messages"`
		Total    int                 `json:"total"`
	}{m, buf.Size()})
	return jsn, err
}

// Size возвращает общее количество сообщений в буфере
func (buf *MessageBuffer) Size() int {
	rs := 0
	buf.RLock()
	for _, v := range buf.m {
		rs += len(v)
	}
	buf.RUnlock()
	return rs
}

// SizeDetailed возвращает массив размеров каждого временного интервала буфера
func (buf *MessageBuffer) SizeDetailed() []int {
	rs := make([]int, len(buf.m))
	buf.RLock()
	for k, v := range buf.m {
		rs[k] = len(v)
	}
	buf.RUnlock()
	return rs
}
